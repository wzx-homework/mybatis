package com.lagou.pojo;

public enum ExecutorType {
    SELECT,
    DELETE,
    UPDATE,
    INSERT
}
